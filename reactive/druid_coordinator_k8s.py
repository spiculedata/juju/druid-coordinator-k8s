from charms.reactive import when, when_not, hook
from charms.reactive import endpoint_from_flag
from charms.reactive.flags import set_flag, get_state, clear_flag
from charmhelpers.core.hookenv import (
    log,
    metadata,
    status_set,
    config,
)

from charms import layer


@when_not('layer.docker-resource.druid_image.fetched')
@when('druid.db.related')
@when('zookeeper.ready')
def fetch_image():
    layer.docker_resource.fetch('druid_image')


@when_not('druid.db.related')
@when_not('druid.configured')
def druid_blocked():
    status_set('blocked', 'Waiting for database')

@when_not('zookeeper.ready')
@when_not('druid.configured')
def druid_zk_blocked():
    status_set('blocked', 'Waiting for zookeeper')

@when('druid.db.related')
@when('zookeeper.ready')
@when('druid.configured')
def druid_active():
    status_set('active', '')


@hook('upgrade-charm')
def upgrade():
    clear_flag('druid.configured')


@when_not('druid.configured')
@when('druid.db.related')
@when('zookeeper.ready')
@when('layer.docker-resource.druid_image.available')
def config_druid():
    dbcfg = get_state('druid.db.config')
    log('got db {0}'.format(dbcfg))

    status_set('maintenance', 'Configuring Druid container')

    spec = make_pod_spec(dbcfg)
    log('set pod spec:\n{}'.format(spec))
    layer.caas_base.pod_spec_set(spec)

    set_flag('druid.configured')


@when('db.available')
@when_not('druid.db.related')
def db_changed():
    log('db available')
    set_flag('druid.db.related')

@when('zookeeper.joined')
@when_not('zookeeper.ready')
def wait_for_zookeeper(zookeeper):
    """
         We always run in Distributed mode, so wait for Zookeeper to become available.
    """
    status_set('waiting', 'Waiting for Zookeeper to become available')

def make_pod_spec(dbcfg):
    image_info = layer.docker_resource.get_info('druid_image')

    with open('reactive/spec_template.yaml') as spec_file:
        pod_spec_template = spec_file.read()

    cfg = config()
    mysql = endpoint_from_flag('db.available')
    jdbc_url = "jdbc://"+mysql.host()+"/"+mysql.database()
    log('building template')
    log(mysql.password())
    log(mysql.user())
    log(configure_zookeepers())
    log(jdbc_url)
    data = {
        'docker_image_path': image_info.registry_path,
        'docker_image_username': image_info.username,
        'docker_image_password': image_info.password,
        'coordinator_port': cfg.get('coordinator_port'),
        'jdbc_url': jdbc_url,
        'db_user': mysql.user(),
        'db_password': mysql.password(),
        'zk_host': configure_zookeepers(),
        'db_type': "mysql"

    }
    return pod_spec_template.format(**data)

@when('mysql.available')
@when_not('druid.db.related')
def mysql_changed():
    log('mysql available')
    set_flag('druid.db.related')

def configure_zookeepers():
    """
        Check for new Zookeeper nodes, add them to list of zookeepers.

    :param zookeeper:
    :return:
    """
    zookeeper = endpoint_from_flag('zookeeper.ready')
    zklist = ''
    for zk_unit in zookeeper.zookeepers():
        zklist += add_zookeeper(zk_unit['host'], zk_unit['port'])
    zklist = zklist[:-1]
    return zklist

def add_zookeeper(host, port):
    """
        Return a ZK hostline for the config.
    """
    return host+':'+port+','

